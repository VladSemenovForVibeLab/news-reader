package com.semenov.newsreader.repository;

import com.semenov.newsreader.models.FeedItem;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedItemRepository extends ListCrudRepository<FeedItem, Long> {
}