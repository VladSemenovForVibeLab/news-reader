package com.semenov.newsreader.controllers;

import com.semenov.newsreader.models.FeedItem;
import com.semenov.newsreader.services.interf.CRUDService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

public abstract class CRUDRestController<E,K> {
    abstract CRUDService<E,K> getService();

    @GetMapping
    public ResponseEntity<List<E>> getAll(){
        List<E> entities = getService().getAll();
        return ResponseEntity.ok(entities);
    }
    @GetMapping("/{id}")
    public ResponseEntity<E> getById(@PathVariable K id){
        Optional<E> entity = getService().getById(id);
        return entity.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
