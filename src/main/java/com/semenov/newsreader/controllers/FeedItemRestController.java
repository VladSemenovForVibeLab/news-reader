package com.semenov.newsreader.controllers;

import com.semenov.newsreader.models.FeedItem;
import com.semenov.newsreader.services.interf.CRUDService;
import com.semenov.newsreader.services.interf.FeedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(FeedItemRestController.FEED_ITEM_REST_URL)
public class FeedItemRestController extends CRUDRestController<FeedItem,Long> {
    public static final String FEED_ITEM_REST_URL="/api/v1/feeditems";

    @Autowired
    FeedItemService feedItemService;
    @Override
    CRUDService<FeedItem, Long> getService() {
        return feedItemService;
    }

    @Override
    public ResponseEntity<FeedItem> getById(Long id) {
        Optional<FeedItem> theItem = feedItemService.getById(id);
        FeedItem item = null;
        if(theItem.isPresent()){
            item=theItem.get();
            return ResponseEntity.ok(item);
        }
        return ResponseEntity.notFound().build();
    }
}
