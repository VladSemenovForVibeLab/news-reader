package com.semenov.newsreader.controllers;

import com.semenov.newsreader.models.FeedItem;
import com.semenov.newsreader.services.interf.FeedItemService;
import jakarta.persistence.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private FeedItemService feedItemService;
    @GetMapping("/")
    public String root(Model model){
        List<FeedItem> feeds = feedItemService.getAll();
        model.addAttribute("feeds",feeds);
        return "index";
    }
    @GetMapping("/feeds")
    public String feedsPage(Model model){
        List<FeedItem> feeds = feedItemService.getAll();
        model.addAttribute("feeds",feeds);
        FeedItem feedItem = new FeedItem();
        model.addAttribute("newFeed",feedItem);
        return "feeds";
    }
}
