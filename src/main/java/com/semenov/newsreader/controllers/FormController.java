package com.semenov.newsreader.controllers;

import com.semenov.newsreader.models.FeedItem;
import com.semenov.newsreader.services.interf.FeedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class FormController {
    @Autowired
    private FeedItemService feedItemService;

    @PostMapping("/feeds/new")
    public String createFeedItem(@ModelAttribute FeedItem feedItem){
        feedItemService.save(feedItem);
        return "redirect:/feeds";
    }
}
