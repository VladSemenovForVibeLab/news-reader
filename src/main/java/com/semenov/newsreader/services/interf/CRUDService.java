package com.semenov.newsreader.services.interf;

import java.util.List;
import java.util.Optional;

public interface CRUDService<E,K> {
    List<E> getAll();

    Optional<E> getById(K id);

    E save(E entity);
}
