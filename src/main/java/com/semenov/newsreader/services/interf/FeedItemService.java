package com.semenov.newsreader.services.interf;

import com.semenov.newsreader.models.FeedItem;

public interface FeedItemService extends CRUDService<FeedItem,Long> {
}
