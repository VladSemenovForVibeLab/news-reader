package com.semenov.newsreader.services.impl;

import com.semenov.newsreader.services.interf.CRUDService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.ListCrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCRUDService<E,K> implements CRUDService<E,K> {
    abstract ListCrudRepository<E,K> getRepository();

    @Override
    public List<E> getAll() {
        List<E> entities = new ArrayList<>();
        getRepository().findAll().forEach(entities::add);
        return entities;
    }

    @Override
    public Optional<E> getById(K id) {
        return getRepository().findById(id);
    }

    @Override
    public E save(E entity) {
        return getRepository().save(entity);
    }
}
