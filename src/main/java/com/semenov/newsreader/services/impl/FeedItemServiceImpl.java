package com.semenov.newsreader.services.impl;

import com.semenov.newsreader.models.FeedItem;
import com.semenov.newsreader.repository.FeedItemRepository;
import com.semenov.newsreader.services.interf.FeedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class FeedItemServiceImpl extends AbstractCRUDService<FeedItem,Long> implements FeedItemService {
    @Autowired
    FeedItemRepository feedItemRepository;
    @Override
    ListCrudRepository<FeedItem, Long> getRepository() {
        return feedItemRepository;
    }

    @Override
    public FeedItem save(FeedItem entity) {
        if(entity.getId()==null){
            entity.setCreatedAt(LocalDateTime.now());
        }
        entity.setUpdatedAt(LocalDateTime.now());
        return feedItemRepository.save(entity);
    }
}
